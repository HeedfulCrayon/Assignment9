package com.heedfulcrayon.nate.cs3270a9;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.heedfulcrayon.nate.cs3270a9.db.Course;

public class MainActivity extends AppCompatActivity implements CourseRecyclerViewAdapter.onCourseClicked, CourseViewFragment.onEditClicked, CourseEditFragment.onDeleteClicked, DeleteCourseDialogFragment.onDeleteCourse{

    private CourseEditFragment courseEditFragment;
    private CourseViewFragment courseViewFragment;
    private AssignmentListFragment assignmentListFragment;
    private CoursesViewModel coursesViewModel;
    private AssignmentsViewModel assignmentsViewModel;
    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        coursesViewModel = ViewModelProviders.of(this).get(CoursesViewModel.class);
        assignmentsViewModel = ViewModelProviders.of(this).get(AssignmentsViewModel.class);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                coursesViewModel.setCourse(null);
                fm = getSupportFragmentManager();
                CourseEditFragment courseEditFragment = new CourseEditFragment();
                fm.beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .add(android.R.id.content,courseEditFragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        fm = getSupportFragmentManager();
        courseEditFragment = (CourseEditFragment)fm.findFragmentByTag("courseEdit");
        courseViewFragment = (CourseViewFragment)fm.findFragmentByTag("courseView");
    }

    @Override
    public void courseClicked(Course course) {
        coursesViewModel.setCourse(course);
        courseViewFragment = new CourseViewFragment();
        fm.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(android.R.id.content,courseViewFragment,"courseView")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void courseLongClicked(String id) {
        assignmentsViewModel.setCourse_id(id);
        assignmentListFragment = new AssignmentListFragment();
        fm.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(android.R.id.content,assignmentListFragment,"assignments")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void editClicked(Course course) {
        coursesViewModel.setCourse(course);
        courseEditFragment = new CourseEditFragment();
        fm.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(android.R.id.content,courseEditFragment,"courseEdit")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void deleteClicked(Course course) {
        DeleteCourseDialogFragment deleteCourseDialogFragment = new DeleteCourseDialogFragment();
        deleteCourseDialogFragment.setCourse(course);
        deleteCourseDialogFragment.show(getSupportFragmentManager(),"deleteCourse");
    }

    @Override
    public void deleteCourse(Course course) {
        courseEditFragment.deleteCourse(course);
    }

    public CoursesViewModel getCoursesViewModel() {
        return coursesViewModel;
    }

    public AssignmentsViewModel getAssignmentsViewModel() {
        return assignmentsViewModel;
    }
}
