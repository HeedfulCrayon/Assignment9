package com.heedfulcrayon.nate.cs3270a9;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.heedfulcrayon.nate.cs3270a9.db.AppDatabase;
import com.heedfulcrayon.nate.cs3270a9.db.Course;

import java.util.ArrayList;
import java.util.List;

public class CourseListFragment extends Fragment {

    private RecyclerView recyclerView;
    private CourseRecyclerViewAdapter adapter;
    private LiveData<List<Course>> courses;
    private int columnCount = 1;
    private View root;
    private GetCanvasCourses task;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_course_list, container, false);
        setHasOptionsMenu(true);
        recyclerView = (RecyclerView)root.findViewById(R.id.rvCourseList);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        Context context = getContext();
        adapter = new CourseRecyclerViewAdapter(new ArrayList<Course>());
        if (columnCount <= 1 ){
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
        else{
            recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));
        }

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(false);

        courses = ((MainActivity)getActivity()).getCoursesViewModel().getCourseList(context);

        courses.observe(this, new Observer<List<Course>>() {
                    @Override
                    public void onChanged(@Nullable List<Course> courses) {
                        if (courses != null){
                            adapter.addItems(courses);
                        }
                    }
                });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_populate_courses:
                task = new GetCanvasCourses();
                task.setOnCoursesPopulated(new GetCanvasCourses.OnCoursesPopulated() {
                    @Override
                    public void processCourseList(final Course[] resultCourses) {
                        if(courses != null){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    AppDatabase.getInstance(getContext()).courseDAO().removeAll();
                                    AppDatabase.getInstance(getContext()).courseDAO().insert(resultCourses);
                                }
                            }).start();
                        }
                    }
                });
                task.execute("");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
