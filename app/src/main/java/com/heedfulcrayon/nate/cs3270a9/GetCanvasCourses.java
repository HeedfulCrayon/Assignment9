package com.heedfulcrayon.nate.cs3270a9;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.heedfulcrayon.nate.cs3270a9.db.Course;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class GetCanvasCourses extends AsyncTask<String,Integer,String>{

    private String rawJSON;
    private OnCoursesPopulated mCallback;

    public interface OnCoursesPopulated {
        void processCourseList(Course[] courses);
    }

    public void setOnCoursesPopulated(OnCoursesPopulated ocp){
        mCallback = ocp;
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            URL url = new URL("https://weber.instructure.com/api/v1/courses?per_page=40");
            HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization","Bearer " + Authorization.AUTH_TOKEN);

            connection.connect();

            int status = connection.getResponseCode();

            switch (status){
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rawJSON = br.readLine();

//                    Log.d("test","rawJson Length: " + rawJSON.length());
//                    Log.d("test","rawJson first 256: " + rawJSON.substring(0,255));

                    break;
            }

        } catch (MalformedURLException e) {
            Log.d("test","Bad URL.  Unable to connect");
        } catch (IOException e) {
            Log.d("test","Unable to access the internet. Check your network connectivity");
        }

        return rawJSON;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        Course[] courses;

        try {
            courses = parseJson(result);
            if (courses != null){
                if (mCallback != null && mCallback instanceof OnCoursesPopulated){
                    mCallback.processCourseList(courses);
                }else {
                    throw new Exception("Must implement OnCoursesPopulated interface");
                }
            }
        }catch (Exception e){
            Log.d("test", e.getMessage());
        }
    }

    private Course[] parseJson(String result) {

        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();

        Course[] courses = null;

        try {
            courses = gson.fromJson(result, Course[].class);
            Log.d("test", "Course Count: " + courses.length);
        }catch (Exception e){
            Log.d("test",e.getMessage());
        }
        return courses;
    }
}
