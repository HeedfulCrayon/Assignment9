package com.heedfulcrayon.nate.cs3270a9.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface CourseDAO {

    @Query("Select * from course")
    LiveData<List<Course>> getAll();

    @Query("select * from course where _id = :id limit 1")
    Course getByID(int id);

    @Update
    void update(Course course);

    @Delete
    void delete(Course course);

    @Insert
    void insert(Course course);

    @Insert
    void insert(Course... couurses);

    @Query("DELETE From course")
    public void removeAll();
}
