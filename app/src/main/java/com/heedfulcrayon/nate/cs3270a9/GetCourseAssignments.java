package com.heedfulcrayon.nate.cs3270a9;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class GetCourseAssignments extends AsyncTask<String, Integer, String> {

    private String rawJSON;
    private OnAssignmentsPopulated mCallback;

    public interface OnAssignmentsPopulated{
        void processAssignments(Assignment[] assignments);
    }

    public void setOnAssignmentsPopulated(OnAssignmentsPopulated oap){
        mCallback = oap;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            URL url = new URL("https://weber.instructure.com/api/v1/courses/" + strings[0] + "/assignments");
            HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization","Bearer " + Authorization.AUTH_TOKEN);

            connection.connect();

            int status = connection.getResponseCode();

            switch (status){
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rawJSON = br.readLine();

//                    Log.d("test","rawJson Length: " + rawJSON.length());
//                    Log.d("test","rawJson first 256: " + rawJSON.substring(0,255));

                    break;
            }

        } catch (MalformedURLException e) {
            Log.d("test","Bad URL.  Unable to connect");
        } catch (IOException e) {
            Log.d("test","Unable to access the internet. Check your network connectivity");
        }

        return rawJSON;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        Assignment[] assignments;

        try {
            assignments = parseJson(s);
            if (assignments != null){
                if(mCallback != null && mCallback instanceof OnAssignmentsPopulated){
                    mCallback.processAssignments(assignments);
                }else{
                    throw new Exception("Must implement OnAssignmentsPopulated");
                }
            }
        }catch (Exception e){
            Log.d("test",e.getMessage());
        }
    }

    private Assignment[] parseJson(String result) {
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        Assignment[] assignments = null;

        try {
            assignments = gson.fromJson(result,Assignment[].class);
        }catch (Exception e){
            Log.d("test", e.getMessage());
        }
        return assignments;
    }
}
