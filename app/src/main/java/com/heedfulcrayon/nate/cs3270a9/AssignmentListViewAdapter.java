package com.heedfulcrayon.nate.cs3270a9;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AssignmentListViewAdapter extends BaseAdapter{

    private final List<Assignment> assignments;

    AssignmentListViewAdapter(List<Assignment> assignments){
        this.assignments = assignments;
    }

    public void addItems(List<Assignment> assignments){
        this.assignments.clear();
        this.assignments.addAll(assignments);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return assignments.size();
    }

    @Override
    public Object getItem(int position) {
        return assignments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return assignments.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.listview_assignment_item,parent,false);
        }
        ((TextView) convertView.findViewById(R.id.tvAssignmentName))
                .setText(((Assignment)getItem(position)).getName());
        ((TextView) convertView.findViewById(R.id.tvAssignmentDueDate))
                .setText(((Assignment)getItem(position)).getDue_at());
        return convertView;
    }
}
