package com.heedfulcrayon.nate.cs3270a9;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.heedfulcrayon.nate.cs3270a9.db.Course;

public class CourseViewFragment extends DialogFragment {

    private View root;
    private Course course;
    private TextView id, name, courseCode, startTime, endTime;

    private onEditClicked mCallback;

    interface onEditClicked{
        void editClicked(Course course);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mCallback = (onEditClicked)context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement onEditClicked interface.");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root =  inflater.inflate(R.layout.fragment_course_view, container, false);

        Toolbar toolbar = (Toolbar)root.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.view_course);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        setHasOptionsMenu(true);

        id = (TextView)root.findViewById(R.id.tvId);
        name = (TextView)root.findViewById(R.id.tvName);
        courseCode = (TextView)root.findViewById(R.id.tvCourseCode);
        startTime = (TextView)root.findViewById(R.id.tvStartTime);
        endTime = (TextView)root.findViewById(R.id.tvEndTime);

        course = ((MainActivity)getActivity()).getCoursesViewModel().getCourse();

        if (course != null){
            id.setText(course.getId());
            name.setText(course.getName());
            courseCode.setText(course.getCourseCode());
            startTime.setText(course.getStartTime());
            endTime.setText(course.getEndTime());
        }

        return root;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.menu_view_dialog, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.actionEdit:
                mCallback.editClicked(course);
                dismiss();
                return true;
            case android.R.id.home:
                dismiss();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
