package com.heedfulcrayon.nate.cs3270a9;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.heedfulcrayon.nate.cs3270a9.db.AppDatabase;
import com.heedfulcrayon.nate.cs3270a9.db.Course;

import java.util.List;

public class CoursesViewModel extends ViewModel {
    private LiveData<List<Course>> courseList;
    private Course course;

    public LiveData<List<Course>> getCourseList(Context context) {
        if(courseList!=null){
            return courseList;
        }
        return courseList = AppDatabase.getInstance(context).courseDAO().getAll();
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
