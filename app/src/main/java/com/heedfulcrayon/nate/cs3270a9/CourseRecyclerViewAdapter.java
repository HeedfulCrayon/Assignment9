package com.heedfulcrayon.nate.cs3270a9;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.heedfulcrayon.nate.cs3270a9.db.Course;

import java.util.List;

public class CourseRecyclerViewAdapter extends RecyclerView.Adapter<CourseRecyclerViewAdapter.ViewHolder> {

    private final List<Course> courses;
    private onCourseClicked mCallback;

    interface onCourseClicked{
        void courseClicked(Course course);
        void courseLongClicked(String id);
    }

    CourseRecyclerViewAdapter(List<Course> courses){
        this.courses = courses;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        try {
            mCallback = (onCourseClicked)recyclerView.getContext();
        }catch (ClassCastException e){
            throw new ClassCastException(recyclerView.getContext().toString() + " must implement onCourseClicked interface");
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_course_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Course course = courses.get(position);
        if(course != null){
            holder.course = course;
            holder.tvName.setText(course.getName());
            holder.tvCourseCode.setText(course.getCourseCode());

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.courseClicked(holder.course);
                }
            });
            holder.view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mCallback.courseLongClicked(holder.course.getId());
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public void addItems(List<Course> courses) {
        this.courses.clear();
        this.courses.addAll(courses);
        notifyDataSetChanged();
    }

    public void clear(){
        this.courses.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvCourseCode;
        public Course course;
        public View view;

        ViewHolder(View itemView){
            super(itemView);
            view = itemView;
            tvName = (TextView)view.findViewById(R.id.tvCourseName);
            tvCourseCode = (TextView)view.findViewById(R.id.tvCourseCode);
        }
    }
}
