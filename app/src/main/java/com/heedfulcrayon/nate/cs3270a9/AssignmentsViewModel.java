package com.heedfulcrayon.nate.cs3270a9;

import android.arch.lifecycle.ViewModel;

public class AssignmentsViewModel extends ViewModel {
    private String course_id;

     public void setCourse_id(String course_id){
        this.course_id = course_id;
     }

     public String getCourse_id(){
        return course_id;
     }
}
