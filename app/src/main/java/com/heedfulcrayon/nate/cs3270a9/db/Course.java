package com.heedfulcrayon.nate.cs3270a9.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Course {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    protected String id;
    protected String name;
    @ColumnInfo(name = "course_code")
    protected String course_code;
    @ColumnInfo(name = "start_time")
    protected String start_at;
    @ColumnInfo(name = "end_time")
    protected String end_at;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseCode() {
        return course_code;
    }

    public void setCourseCode(String courseCode) {
        this.course_code = courseCode;
    }

    public String getStartTime() {
        return start_at;
    }

    public void setStartTime(String startTime) {
        this.start_at = startTime;
    }

    public String getEndTime() {
        return end_at;
    }

    public void setEndTime(String endTime) {
        this.end_at = endTime;
    }

    public Course(int _id, String id, String name, String course_code, String start_at, String end_at) {
        this._id = _id;
        this.id = id;
        this.name = name;
        this.course_code = course_code;
        this.start_at = start_at;
        this.end_at = end_at;
    }

    @Ignore
    public Course(String id, String name, String course_code, String start_at, String end_at) {
        this.id = id;
        this.name = name;
        this.course_code = course_code;
        this.start_at = start_at;
        this.end_at = end_at;
    }

    @Override
    public String toString() {
        return "Course{" +
                "_id=" + _id +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", courseCode='" + course_code + '\'' +
                ", startTime='" + start_at + '\'' +
                ", endTime='" + end_at + '\'' +
                '}';
    }
}
