package com.heedfulcrayon.nate.cs3270a9;


import android.app.Dialog;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AssignmentListFragment extends DialogFragment {

    private View root;
    private ListView listView;
    private AssignmentListViewAdapter adapter;
    private LiveData<List<Assignment>> assignments;
    private int columnCount = 1;
    private AssignmentsViewModel assignmentsViewModel;
    private String course_id;
    private GetCourseAssignments task;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_assignment_list, container, false);
        listView = (ListView)root.findViewById(R.id.lvAssignmentList);

        Toolbar toolbar = (Toolbar)root.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.view_assignments);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        Context context = getContext();
        adapter = new AssignmentListViewAdapter(new ArrayList<Assignment>());
        listView.setAdapter(adapter);
        assignmentsViewModel = ((MainActivity)getActivity()).getAssignmentsViewModel();

        task = new GetCourseAssignments();
        task.setOnAssignmentsPopulated(new GetCourseAssignments.OnAssignmentsPopulated() {
            @Override
            public void processAssignments(Assignment[] assignments) {
               if (assignments != null){
                   adapter.addItems(Arrays.asList(assignments));
               }
            }
        });
        task.execute(assignmentsViewModel.getCourse_id());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                dismiss();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
