package com.heedfulcrayon.nate.cs3270a9;


public class Assignment {

    protected Integer id;
    protected String name;
    protected String description;
    protected String created_at;
    protected String due_at;
    protected String course_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDue_at() {
        return due_at;
    }

    public void setDue_at(String due_at) {
        this.due_at = due_at;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public Assignment(Integer id, String name, String description, String created_at, String due_at) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.created_at = created_at;
        this.due_at = due_at;
    }
}
