package com.heedfulcrayon.nate.cs3270a9;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.heedfulcrayon.nate.cs3270a9.db.Course;

public class DeleteCourseDialogFragment extends DialogFragment {

    private Course course;
    private onDeleteCourse mCallback;

    interface onDeleteCourse{
        void deleteCourse(Course course);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mCallback = (onDeleteCourse)context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement onDeleteCourse interface.");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        course = ((MainActivity)getActivity()).getCoursesViewModel().getCourse();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title_delete_course)
                .setMessage(R.string.delete_message)
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.deleteCourse(course);
                        dismiss();
                    }
                });
        return builder.create();
    }

    public void setCourse(Course course){
        this.course = course;
    }
}
