package com.heedfulcrayon.nate.cs3270a9;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.heedfulcrayon.nate.cs3270a9.db.AppDatabase;
import com.heedfulcrayon.nate.cs3270a9.db.Course;


/**
 * A simple {@link Fragment} subclass.
 */
public class CourseEditFragment extends DialogFragment {

    private Course course;
    private TextInputEditText id, name, courseCode, startTime, endTime;
    private View root;

    private onDeleteClicked mCallback;

    interface onDeleteClicked{
        void deleteClicked(Course course);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (onDeleteClicked)context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implement onDeleteClicked interface.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_course_edit,container, false);

        Toolbar toolbar = (Toolbar)root.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.new_course);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel);
        }

        setHasOptionsMenu(true);

        id = (TextInputEditText)root.findViewById(R.id.newcourseID);
        name = (TextInputEditText)root.findViewById(R.id.newcourseName);
        courseCode = (TextInputEditText)root.findViewById(R.id.newcourseCode);
        startTime = (TextInputEditText)root.findViewById(R.id.newstartTime);
        endTime = (TextInputEditText)root.findViewById(R.id.newendTime);

        course = ((MainActivity)getActivity()).getCoursesViewModel().getCourse();

        if (course != null){
            id.setText(course.getId());
            name.setText(course.getName());
            courseCode.setText(course.getCourseCode());
            startTime.setText(course.getStartTime());
            endTime.setText(course.getEndTime());
        }

        return root;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (course != null) {
            getActivity().getMenuInflater().inflate(R.menu.menu_edit_dialog, menu);
        }else{
            getActivity().getMenuInflater().inflate(R.menu.menu_create_dialog, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.actionSave:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (course == null) {
                            course = new Course(id.getText().toString(), name.getText().toString(), courseCode.getText().toString(), startTime.getText().toString(), endTime.getText().toString());
                            AppDatabase.getInstance(getContext())
                                    .courseDAO()
                                    .insert(course);
                        }else{
                            course.setId(id.getText().toString());
                            course.setName(name.getText().toString());
                            course.setCourseCode(courseCode.getText().toString());
                            course.setStartTime(startTime.getText().toString());
                            course.setEndTime(endTime.getText().toString());
                            AppDatabase.getInstance(getContext())
                                    .courseDAO()
                                    .update(course);
                        }
                    }
                }).start();
                dismiss();
                return true;
            case R.id.actionDelete:
                mCallback.deleteClicked(course);
                return true;
            case android.R.id.home:
                dismiss();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void deleteCourse(final Course course) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(getContext())
                        .courseDAO()
                        .delete(course);
            }
        }).start();
        dismiss();
    }

}
